import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import { PapperBlock } from 'dan-components';
import BasicMarker from './BasicMarker';
import FirebaseApp from '../../config/FirebaseApp';

class MapMarker extends React.Component {
  render() {
    const title = brand.name + ' - Map';
    const description = brand.desc;
    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
        </Helmet>
        <PapperBlock overflowX title="Map with a Marker" desc="A sample for basic mark a coodinate in map">
          <FirebaseApp />
        </PapperBlock>
      </div>
    );
  }
}

export default MapMarker;
