import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import { PapperBlock } from 'dan-components';
import BarSimple from './BarSimple';

class BarCharts extends React.Component {
  render() {
    const title = brand.name + ' - Sitios';
    const description = brand.desc;
    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
        </Helmet>
        <PapperBlock title="Sitios" icon="ios-stats-outline" desc="" overflowX>
          <div>
            <BarSimple />
          </div>
        </PapperBlock>
      </div>
    );
  }
}

export default BarCharts;
