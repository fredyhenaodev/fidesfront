import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import dummy from 'dan-api/dummy/dummyContents';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
  Cover,
  PapperBlock
} from 'dan-components';
import bgCover from 'dan-images/petal_bg.svg';
import styles from 'dan-components/SocialMedia/jss/cover-jss';
import Grid from '@material-ui/core/Grid';
import PieCustomShape from './PieCustomShape';

function TabContainer(props) {
  const { children } = props;
  return (
    <div style={{ paddingTop: 8 * 3 }}>
      {children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

class UserProfile extends React.Component {
  render() {
    const title = brand.name + ' - Profile';
    const description = brand.desc;
    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
        </Helmet>
        <Grid container spacing={24}>
          <Grid item md={6} xs={12}>
            <Cover
              coverImg={bgCover}
              avatar={dummy.user.avatar}
              name={dummy.user.name}
              desc="Decameron"
          />
          </Grid>
          <Grid item md={6} xs={12}>
            <PapperBlock title="Mis Sitios" icon="ios-pie-outline" desc="" overflowX>
              <div>
                <PieCustomShape />
              </div>
            </PapperBlock>
          </Grid>
        </Grid>
      </div>
    );
  }
}

UserProfile.propTypes = {
};

const mapStateToProps = state => ({
  force: state, // force state from reducer
});

const UserProfileMapped = connect(
  mapStateToProps
)(UserProfile);

export default withStyles(styles)(UserProfileMapped);
