import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import { NavLink } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import ArrowForward from "@material-ui/icons/ArrowForward";
import Paper from "@material-ui/core/Paper";
import Icon from "@material-ui/core/Icon";
import brand from "dan-api/dummy/brand";
import logo from "dan-images/logo.svg";
import styles from "./user-jss";

/**
 * Form Formik
 */
import { Formik, Form, Field } from "formik";
import { TextField} from "formik-material-ui";


class Form extends React.Component {
  state = {
    showPassword: false
  };

  handleClickShowPassword = () => {
    const { showPassword } = this.state;
    this.setState({ showPassword: !showPassword });
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  render() {
    const { classes, handleSubmit, pristine, submitting, deco, onSubmitLogin } = this.props;
    const { showPassword } = this.state;
    return (
      <Paper className={classNames(classes.sideWrap, deco && classes.petal)}>
        <div className={classes.topBar}>
          <NavLink to="/" className={classes.brand}>
            <img src={logo} alt={brand.name} />
            {brand.name}
          </NavLink>
          <Button
            size="small"
            className={classes.buttonLink}
            component={NavLink}
            to="/register"
          >
            <Icon className={classes.icon}>arrow_forward</Icon>
            Crear una nueva cuenta
          </Button>
        </div>
        <Typography variant="h4" className={classes.title} gutterBottom>
          Ingresar
        </Typography>
        <section className={classes.pageFormSideWrap}>
          <Formik
            onSubmit={(values, actions) => {
              // same shape as initial values
              actions.setSubmitting(false);
              console.log(values);
              onSubmitLogin(values);
            }}
          >
            {() => (
              <Form>
                <div>
                  <FormControl className={classes.formControl}>
                    <Field
                      name="gender"
                      type="text"
                      component={TextField}
                      placeholder="Genero"
                      label="Tipo de persona"
                      className={classes.field}
                    />
                  </FormControl>
                </div>
                <div>
                <FormControl className={classes.formControl}>
                    <Field
                      name="site"
                      type="text"
                      component={TextField}
                      placeholder="Sitio"
                      label="Tipo de sitio"
                      className={classes.field}
                    />
                  </FormControl>
                </div>
                <div className={classes.optArea}>
                </div>
                <div className={classes.btnArea}>
                  <Button
                    variant="contained"
                    fullWidth
                    color="primary"
                    size="large"
                    type="submit"
                  >
                    Continuar
                    <ArrowForward
                      className={classNames(
                        classes.rightIcon,
                        classes.iconSmall
                      )}
                      disabled={submitting || pristine}
                    />
                  </Button>
                </div>
              </Form>
            )}
          </Formik>
        </section>
      </Paper>
    );
  }
}

Form.propTypes = {
  classes: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  deco: PropTypes.bool.isRequired
};

export default withStyles(styles)(Form);
