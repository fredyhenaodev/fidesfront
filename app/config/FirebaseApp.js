import React from 'react';
import confiFirebase from './firebase';

class FirebaseApp extends React.Component {
    state = {
      name: ''
    };

    componentDidMount = () => {
      const nameRef = confiFirebase.database().ref('/users/').child('name');
      nameRef.on('value', snapshot => {
        this.setState({
          name: snapshot.val()
        });
      });
      /* firebase.database().ref('/users/').once('value').then((snapshot) => {
        this.setState({ name: snapshot.val().name });
      }); */
    }

    render() {
      const { name } = this.state;
      return (
        <h1>{ name }</h1>
      );
    }
}

export default FirebaseApp;
